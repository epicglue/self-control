package self_control

import "time"

const (
	DefaultCleanUpInterval = 4 * time.Hour
)

type Store interface {
	Get(key string) *Limit
	Set(key string, value *Limit)
}
