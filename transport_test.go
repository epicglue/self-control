package self_control_test

import (
	"github.com/epic-glue/self-control"
	"github.com/spf13/cast"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
	"time"
)

var fakeResponse string = `[]`

func TestNewTokenLimitFromHeader(t *testing.T) {
	// Invalid headers
	h := http.Header{}
	limit := self_control.NewTokenLimitFromHeader(h)

	assert.Nil(t, limit)

	// Remaining
	h = http.Header{}
	h.Add("X-RateLimit-Remaining", "111")
	limit = self_control.NewTokenLimitFromHeader(h)

	assert.NotNil(t, limit)
	assert.Equal(t, 111, limit.Remaining)
	assert.Nil(t, limit.ResetAt)

	h = http.Header{}
	h.Add("X-Rate-Limit-Remaining", "121")
	limit = self_control.NewTokenLimitFromHeader(h)

	assert.NotNil(t, limit)
	assert.Equal(t, 121, limit.Remaining)
	assert.Nil(t, limit.ResetAt)

	// Reset
	h = http.Header{}
	h.Add("X-RateLimit-Remaining", "111")
	h.Add("X-RateLimit-Reset", "1471502318")
	limit = self_control.NewTokenLimitFromHeader(h)

	assert.NotNil(t, limit)
	assert.Equal(t, 111, limit.Remaining)
	assert.NotNil(t, limit.ResetAt)
	assert.Equal(t, time.Date(2016, 8, 18, 6, 38, 38, 0, time.UTC), *limit.ResetAt)

	h = http.Header{}
	h.Add("X-Rate-Limit-Remaining", "111")
	h.Add("X-Rate-Limit-Reset", "1471502318")
	limit = self_control.NewTokenLimitFromHeader(h)

	assert.NotNil(t, limit)
	assert.Equal(t, 111, limit.Remaining)
	assert.NotNil(t, limit.ResetAt)
	assert.Equal(t, time.Date(2016, 8, 18, 6, 38, 38, 0, time.UTC), *limit.ResetAt)

	h = http.Header{}
	h.Add("X-Rate-Limit-Remaining", "111")
	h.Add("X-Rate-Limit-Reset", "1471502318000000000")
	limit = self_control.NewTokenLimitFromHeader(h)

	assert.NotNil(t, limit)
	assert.Equal(t, 111, limit.Remaining)
	assert.NotNil(t, limit.ResetAt)
	assert.Equal(t, time.Date(2016, 8, 18, 6, 38, 38, 0, time.UTC), *limit.ResetAt)

	resetIn5 := time.Now().Add(5 * time.Minute)
	resetIn5 = resetIn5.Truncate(time.Second)

	h = http.Header{}
	h.Add("X-Rate-Limit-Remaining", "111")
	h.Add("X-Rate-Limit-Reset", "300")
	limit = self_control.NewTokenLimitFromHeader(h)

	assert.NotNil(t, limit)
	assert.Equal(t, 111, limit.Remaining)
	assert.NotNil(t, limit.ResetAt)
	assert.Equal(t, resetIn5.UTC(), *limit.ResetAt)
}

func TestTransport_RoundTrip(t *testing.T) {
	req, err := http.NewRequest(http.MethodGet, "https://hacker-news.firebaseio.com/v0/topstories.json", nil)
	if err != nil {
		t.Fatal(err)
	}

	resetAt := time.Now().Add(5 * time.Minute)

	c := self_control.FakeHttpClient(
		http.StatusOK,
		map[string]string{
			"X-RateLimit-Remaining": "1",
			"X-RateLimit-Reset":     cast.ToString(resetAt.Unix()),
		},
		fakeResponse)

	resp, err := c.Do(req)
	assert.Nil(t, err)
	assert.Equal(t, resp.StatusCode, http.StatusOK)

	c = self_control.FakeHttpClient(
		http.StatusOK,
		map[string]string{
			"X-RateLimit-Remaining": "0",
			"X-RateLimit-Reset":     cast.ToString(resetAt.Unix()),
		},
		fakeResponse)

	resp, err = c.Do(req)
	assert.Nil(t, err)
	assert.Equal(t, resp.StatusCode, http.StatusOK)

	c = self_control.FakeHttpClient(
		http.StatusTooManyRequests,
		map[string]string{},
		fakeResponse)

	resp, err = c.Do(req)
	assert.Nil(t, err)
	assert.Equal(t, resp.StatusCode, http.StatusTooManyRequests)
}
