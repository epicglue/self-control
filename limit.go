package self_control

import (
	"github.com/spf13/cast"
	"net/http"
	"time"
)

type Limit struct {
	Remaining int
	ResetAt   *time.Time
}

func NewTokenLimitFromHeader(h http.Header) *Limit {
	limit := Limit{}

	// Number of remaining tokens
	found := false
	for _, k := range []string{
		"X-RateLimit-Remaining",
		"X-Rate-Limit-Remaining",
	} {
		if h.Get(k) != "" {
			limit.Remaining = cast.ToInt(h.Get(k))

			found = true
			break
		}
	}

	if !found {
		return nil
	}

	// Reset date
	for _, k := range []string{
		"X-RateLimit-Reset",
		"X-Rate-Limit-Reset",
	} {
		if h.Get(k) != "" {
			limit.ResetAt = parseEpoch(h.Get(k))
			break
		}
	}

	return &limit
}

func parseEpoch(value string) *time.Time {
	currentEpochNano := cast.ToString(time.Now().UnixNano())
	currentEpoch := cast.ToString(time.Now().Unix())
	resetTimestamp := cast.ToInt64(value)
	v := time.Time{}

	// Try EPOCH
	if len(currentEpochNano) <= len(value) {
		// Assume it's EPOCH with milliseconds and convert it to time
		v = time.Unix(resetTimestamp/1000000000, 0)
	} else if len(currentEpoch) <= len(value) {
		// Assume it's EPOCH and convert it to time
		v = time.Unix(resetTimestamp, 0)
	} else {
		// Use current time + number of seconds (hopefully) left provided by API
		v = time.Unix(cast.ToInt64(currentEpoch)+resetTimestamp, 0)
	}

	v = v.UTC()
	return &v
}
