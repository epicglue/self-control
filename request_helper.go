package self_control

import (
	"net/http"
	"strings"
)

func (t *Transport) buildKeyForStore(request *http.Request) string {
	token := ""
	if request.Header != nil {
		auth := request.Header.Get("Authorization")
		if strings.HasPrefix(auth, "Bearer") {
			token = strings.Replace(auth, "Bearer ", "", -1)
		}
	}

	key := request.URL.Host

	if token != "" {
		key = key + ":" + token
	}

	return key
}
