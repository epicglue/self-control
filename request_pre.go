package self_control

import (
	"errors"
	"fmt"
	"github.com/uber-go/zap"
	"net/http"
	"time"
)

const (
	ERR_NO_TOKENS_LEFT = "No tokens left"
)

func (t *Transport) pre(req *http.Request) error {
	if !t.checkLimits(req) {
		return errors.New(ERR_NO_TOKENS_LEFT)
	}

	return nil
}

// Get limits
func (t *Transport) checkLimits(request *http.Request) bool {
	t.Log.Info("Cache.GET", zap.String("key", t.buildKeyForStore(request)))

	limit := t.Store.Get(t.buildKeyForStore(request))

	if limit == nil {
		return true
	}

	if limit.Remaining > 0 {
		t.Log.Info(fmt.Sprintf("Has %d tokens left", limit.Remaining))
		return true
	}

	if limit.ResetAt != nil && limit.ResetAt.Unix() > 0 && limit.ResetAt.Before(time.Now()) {
		t.Log.Info("Previous token expired", zap.Time("now", time.Now()), zap.Time("token_reset_at", *limit.ResetAt))
		return true
	}

	t.Log.Info("No tokens left")

	return false
}
