package self_control

import (
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"path"
)

// Use for mock responses
func FakeHttpClient(code int, header map[string]string, body string) *http.Client {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		for k, v := range header {
			w.Header().Set(k, v)
		}
		w.WriteHeader(code)
		io.WriteString(w, body)
	}))

	u, _ := url.Parse(server.URL)
	proxyTransport := &rewriteTransport{
		URL: u,
	}

	transport := &Transport{
		RequestTransport: proxyTransport,
	}

	return &http.Client{
		Transport: transport,
	}
}

// http://stackoverflow.com/questions/27880930/mocking-https-responses-in-go

// RewriteTransport is an http.RoundTripper that rewrites requests
// using the provided URL's Scheme and Host, and its Path as a prefix.
// The Opaque field is untouched.
// If Transport is nil, http.DefaultTransport is used
type rewriteTransport struct {
	Transport http.RoundTripper
	URL       *url.URL
}

func (t rewriteTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	// note that url.URL.ResolveReference doesn't work here
	// since t.u is an absolute url
	req.URL.Scheme = t.URL.Scheme
	req.URL.Host = t.URL.Host
	req.URL.Path = path.Join(t.URL.Path, req.URL.Path)
	rt := t.Transport
	if rt == nil {
		rt = http.DefaultTransport
	}
	return rt.RoundTrip(req)
}
