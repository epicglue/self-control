package self_control

import "net/http"

func NoTokensResponse() *http.Response {
	r := http.Response{
		StatusCode: http.StatusTooManyRequests,
	}
	r.Header.Set("X-Origin", "SelfControl")
	return &r
}
