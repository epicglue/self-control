package self_control

import (
	"github.com/uber-go/zap"
	"net/http"
)

var DefaultTransport http.RoundTripper = http.DefaultTransport

type Transport struct {
	//
	RequestTransport http.RoundTripper

	//
	Store Store

	// Set up logging
	Log           zap.Logger
	EnableLogging bool
	LoggingLevel  zap.Level
}

func (t *Transport) RoundTrip(req *http.Request) (*http.Response, error) {
	t.setDefaults()

	if err := t.pre(req); err != nil {
		t.Log.Error(err.Error())
		// TODO: return err here?
		// TODO: recognize type of error and provide appropriate response
		return NoTokensResponse(), err
	}

	// Use default or provided Transport to make request
	resp, err := t.RequestTransport.RoundTrip(req)

	if err != nil {
		t.Log.Error(err.Error())
		return resp, err
	}

	t.Log.Info(req.URL.String(), zap.String("status", resp.Status), zap.Int("code", resp.StatusCode))

	t.post(resp)

	return resp, err
}

func (t *Transport) setDefaults() {
	if t.RequestTransport == nil {
		t.RequestTransport = DefaultTransport
	}

	if t.Store == nil {
		t.Store = NewMemoryStore()
	}

	t.Log = zap.New(zap.NewJSONEncoder(zap.NoTime()))
	if t.EnableLogging {
		// TODO: recognize if level wasn't provided
		t.Log.SetLevel(t.LoggingLevel)
	} else {
		t.Log.SetLevel(zap.DebugLevel)
	}
}
