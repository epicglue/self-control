package self_control

import (
	"github.com/uber-go/zap"
	"net/http"
)

func (t *Transport) post(res *http.Response) {
	t.updateLimits(res)
}

// Store new limits
func (t *Transport) updateLimits(response *http.Response) {
	if limit := NewTokenLimitFromHeader(response.Header); limit != nil {
		t.Log.Info("Cache.SET", zap.String("key", t.buildKeyForStore(response.Request)), zap.Int("remaining", limit.Remaining))

		t.Store.Set(t.buildKeyForStore(response.Request), limit)
	}
}
